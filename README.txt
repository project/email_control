INSTALLATION
------------

 * Enable Email Control module
 * Navigate to the admin page at Configuration -> System -> Email Control
 * Set all the settings


OPTIONALLY HARD-CODE SETTINGS PER ENVIRONMENT VIA SETTINGS.PHP
--------------------------------------------------------------

All of the administrative settings on the configuration page can be overridden
 in settings.php; e.g.:

```
$conf['email_control_send'] = 'custom';
$conf['email_control_send_email_address'] = 'MyEmail@MyDomain.com';
$conf['email_control_send_email_name'] = 'My Default Username';
```

See the content of the `email_control_config_form()` function for a full list of
all the variable names and values that may be overwritten.
